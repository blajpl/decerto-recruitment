package pl.blaj.decertorecruitment.api.quote.controller;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.stream.Collectors;
import java.util.stream.LongStream;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import pl.blaj.decertorecruitment.api.quote.dto.QuoteResponse;
import pl.blaj.decertorecruitment.api.quote.dto.QuoteResponse.AuthorResponse;
import pl.blaj.decertorecruitment.api.quote.service.QuoteCrudService;

@WebMvcTest(QuoteController.class)
public class QuoteControllerTest {

  @Autowired private MockMvc mvc;
  @Autowired private ObjectMapper objectMapper;

  @MockBean private QuoteCrudService quoteCrudService;

  @Test
  public void whenQuotesExisting_listQuotes_shouldReturnJsonArray() throws Exception {
    // given
    var quotesResponsesList =
        LongStream.range(1, 5)
            .mapToObj(
                i ->
                    new QuoteResponse()
                        .setContent("content#" + i)
                        .setAuthor(authorResponse("first_name#" + i, "last_name#" + i)))
            .collect(Collectors.toList());

    given(quoteCrudService.getAllQuotesResponses()).willReturn(quotesResponsesList);

    // when
    var response = mvc.perform(get("/quote").contentType(MediaType.APPLICATION_JSON));

    // then
    response
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(jsonPath("$", hasSize(quotesResponsesList.size())))
        .andExpect(jsonPath("$[0].content", is("content#1")));
  }

  @Test
  public void whenExistingQuoteId_getQuote_shouldReturnJson() throws Exception {
    // given
    var existingQuoteId = 1L;

    given(quoteCrudService.getOneQuoteResponse(existingQuoteId))
        .willReturn(
            new QuoteResponse()
                .setContent("content")
                .setAuthor(authorResponse("first_name", "last_name")));

    var response =
        mvc.perform(
            get("/quote/{quoteId}", existingQuoteId).contentType(MediaType.APPLICATION_JSON));

    // then
    response
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(jsonPath("$.content", is("content")))
        .andExpect(jsonPath("$.author.firstName", is("first_name")))
        .andExpect(jsonPath("$.author.lastName", is("last_name")));
  }

  private AuthorResponse authorResponse(String firstName, String lastName) {
    return new AuthorResponse().setFirstName(firstName).setLastName(lastName);
  }
}
