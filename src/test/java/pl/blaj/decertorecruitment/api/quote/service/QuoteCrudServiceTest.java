package pl.blaj.decertorecruitment.api.quote.service;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Optional;
import javax.persistence.EntityNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.blaj.decertorecruitment.api.quote.dto.AuthorRequest;
import pl.blaj.decertorecruitment.api.quote.dto.QuoteUpdateRequest;
import pl.blaj.decertorecruitment.api.quote.persistence.entity.Quote;
import pl.blaj.decertorecruitment.api.quote.persistence.repository.QuoteRepository;

@ExtendWith(MockitoExtension.class)
public class QuoteCrudServiceTest {

  private QuoteCrudService crudService;

  @Mock private QuoteRepository quoteRepository;

  @BeforeEach
  public void setUp() {
    crudService = new QuoteCrudService(quoteRepository);
  }

  @Test
  public void whenQuoteExists_update_shouldSaveQuote() {
    // given
    var existingQuoteId = 123L;
    var quoteUpdateRequest = quoteUpdateRequest();
    var quote = new Quote();

    given(quoteRepository.findById(existingQuoteId)).willReturn(Optional.of(quote));
    given(quoteRepository.save(any(Quote.class))).willReturn(quote);

    // when
    crudService.update(existingQuoteId, quoteUpdateRequest);

    // then
    verify(quoteRepository, times(1)).save(quote);
  }

  @Test
  public void whenQuoteNotExists_update_shouldThrowException() {
    // given
    var nonExistingQuoteId = 123L;

    given(quoteRepository.findById(nonExistingQuoteId)).willReturn(Optional.empty());

    // when
    assertThrows(
        EntityNotFoundException.class,
        () -> crudService.update(nonExistingQuoteId, quoteUpdateRequest()));
  }

  private QuoteUpdateRequest quoteUpdateRequest() {
    return new QuoteUpdateRequest()
        .setContent("content123")
        .setAuthor(new AuthorRequest().setFirstName("author").setLastName("last name"));
  }
}
