package pl.blaj.decertorecruitment.api.quote.persistence.repository;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import pl.blaj.decertorecruitment.api.quote.persistence.entity.Quote;
import pl.blaj.decertorecruitment.api.quote.persistence.entity.Quote.Author;

@DataJpaTest
public class QuoteRepositoryTest {

  @Autowired private QuoteRepository quoteRepository;

  @Test
  public void whenEntityExists_findById_shouldReturnEntity() {
    // given
    var quote =
        new Quote()
            .setContent("content")
            .setAuthor(new Author().setFirstName("first name").setLastName("last name"));

    var existingId = quoteRepository.save(quote).getId();

    // when
    var entity = quoteRepository.findById(existingId);

    // then
    assertThat(entity).isNotEmpty();
    assertThat(entity.map(Quote::getId)).contains(existingId);
  }

  @Test
  public void whenEntityNotExists_findById_shouldReturnEmpty() {
    // given
    var nonExistingId = 123L;

    // when
    var entity = quoteRepository.findById(nonExistingId);

    // then
    assertThat(entity).isEmpty();
  }
}
