package pl.blaj.decertorecruitment.api.quote.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.blaj.decertorecruitment.api.quote.dto.AuthorRequest;
import pl.blaj.decertorecruitment.api.quote.dto.QuoteCreateRequest;
import pl.blaj.decertorecruitment.api.quote.dto.QuoteResponse;
import pl.blaj.decertorecruitment.api.quote.dto.QuoteUpdateRequest;
import pl.blaj.decertorecruitment.api.quote.mapper.QuoteResponseMapper;
import pl.blaj.decertorecruitment.api.quote.persistence.entity.Quote;
import pl.blaj.decertorecruitment.api.quote.persistence.entity.Quote.Author;
import pl.blaj.decertorecruitment.api.quote.persistence.repository.QuoteRepository;

@Service
@RequiredArgsConstructor
public class QuoteCrudService {

  private final QuoteRepository quoteRepository;

  public List<QuoteResponse> getAllQuotesResponses() {
    return quoteRepository.findAll().map(QuoteResponseMapper::map).collect(Collectors.toList());
  }

  public QuoteResponse getOneQuoteResponse(Long id) {
    return quoteRepository
        .findById(id)
        .map(QuoteResponseMapper::map)
        .orElseThrow(() -> new EntityNotFoundException("Quote with id " + id + " not exists."));
  }

  @Transactional
  public void create(QuoteCreateRequest request) {
    quoteRepository.save(
        new Quote().setContent(request.getContent()).setAuthor(getAuthor(request.getAuthor())));
  }

  @Transactional
  public void update(Long id, QuoteUpdateRequest request) {
    var quote = fetchQuote(id);

    quoteRepository.save(
        quote.setContent(request.getContent()).setAuthor(getAuthor(request.getAuthor())));
  }

  @Transactional
  public void delete(Long id) {
    quoteRepository.deleteById(fetchQuote(id).getId());
  }

  private Author getAuthor(AuthorRequest authorRequest) {
    return Optional.ofNullable(authorRequest)
        .map(ar -> new Author().setFirstName(ar.getFirstName()).setLastName(ar.getLastName()))
        .orElse(null);
  }

  private Quote fetchQuote(Long id) {
    return quoteRepository
        .findById(id)
        .orElseThrow(() -> new EntityNotFoundException("Quote with id " + id + " not exists."));
  }
}
