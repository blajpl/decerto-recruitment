package pl.blaj.decertorecruitment.api.quote.controller;

import java.util.List;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import pl.blaj.decertorecruitment.api.quote.dto.QuoteCreateRequest;
import pl.blaj.decertorecruitment.api.quote.dto.QuoteResponse;
import pl.blaj.decertorecruitment.api.quote.dto.QuoteUpdateRequest;
import pl.blaj.decertorecruitment.api.quote.service.QuoteCrudService;

@RestController
@RequestMapping("/quote")
@RequiredArgsConstructor
public class QuoteController {

  private final QuoteCrudService quoteCrudService;

  @GetMapping
  public List<QuoteResponse> listQuotes() {
    return quoteCrudService.getAllQuotesResponses();
  }

  @GetMapping("/{id}")
  public QuoteResponse getQuote(@PathVariable Long id) {
    return quoteCrudService.getOneQuoteResponse(id);
  }

  @PostMapping
  @ResponseStatus(HttpStatus.OK)
  public void createQuote(@RequestBody @Valid QuoteCreateRequest request) {
    quoteCrudService.create(request);
  }

  @PutMapping("/{id}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void updateQuote(@PathVariable Long id, @RequestBody @Valid QuoteUpdateRequest request) {
    quoteCrudService.update(id, request);
  }

  @DeleteMapping("/{id}")
  @ResponseStatus(HttpStatus.ACCEPTED)
  public void deleteQuote(@PathVariable Long id) {
    quoteCrudService.delete(id);
  }
}
