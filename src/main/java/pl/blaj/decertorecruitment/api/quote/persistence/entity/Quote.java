package pl.blaj.decertorecruitment.api.quote.persistence.entity;

import com.sun.istack.NotNull;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.hibernate.annotations.Where;
import pl.blaj.decertorecruitment.common.persistence.entity.AuditingEntity;

@Table(name = "quote")
@Entity
@Data
@ToString(onlyExplicitlyIncluded = true)
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = true)
@Accessors(chain = true)
@Where(clause = "deleted = false")
public class Quote extends AuditingEntity {

  @NotNull
  @Size(min = 3)
  private String content;

  @NotNull
  private Author author = new Author();

  @Embeddable
  @Data
  @Accessors(chain = true)
  public static class Author {

    @NotNull
    @Size(min = 3, max = 50)
    @Column(name = "author_first_name")
    private String firstName;

    @NotNull
    @Size(min = 3, max = 50)
    @Column(name = "author_last_name")
    private String lastName;
  }
}
