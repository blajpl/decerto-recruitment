package pl.blaj.decertorecruitment.api.quote.dto;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class QuoteResponse {

  private Long id;

  private String content;

  private AuthorResponse author;

  @Data
  @Accessors(chain = true)
  public static class AuthorResponse {

    private String firstName;

    private String lastName;
  }
}
