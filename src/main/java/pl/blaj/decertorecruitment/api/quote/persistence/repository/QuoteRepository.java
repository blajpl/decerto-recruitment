package pl.blaj.decertorecruitment.api.quote.persistence.repository;

import java.util.Optional;
import java.util.stream.Stream;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;
import pl.blaj.decertorecruitment.api.quote.persistence.entity.Quote;

@org.springframework.stereotype.Repository
public interface QuoteRepository extends Repository<Quote, Long> {

  Optional<Quote> findById(Long id);

  Stream<Quote> findAll();

  Quote save(Quote quote);

  @Modifying
  @Query("UPDATE #{#entityName} e SET e.deleted = true, e.deletedAt = current_timestamp WHERE e.id = :id")
  void deleteById(@Param("id") Long id);
}
