package pl.blaj.decertorecruitment.api.quote.dto;

import com.sun.istack.NotNull;
import javax.validation.constraints.Size;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class AuthorRequest {

  @NotNull
  @Size(min = 3, max = 50)
  private String firstName;

  @NotNull
  @Size(min = 3, max = 50)
  private String lastName;
}
