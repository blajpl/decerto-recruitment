package pl.blaj.decertorecruitment.api.quote.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class QuoteUpdateRequest {

  @NotNull
  @Size(min = 3)
  private String content;

  @NotNull
  private AuthorRequest author;
}
