package pl.blaj.decertorecruitment.api.quote.mapper;

import java.util.Optional;
import lombok.experimental.UtilityClass;
import pl.blaj.decertorecruitment.api.quote.dto.QuoteResponse;
import pl.blaj.decertorecruitment.api.quote.dto.QuoteResponse.AuthorResponse;
import pl.blaj.decertorecruitment.api.quote.persistence.entity.Quote;

@UtilityClass
public class QuoteResponseMapper {

  public static QuoteResponse map(Quote quote) {
    return Optional.ofNullable(quote)
        .map(
            q ->
                new QuoteResponse()
                    .setId(q.getId())
                    .setContent(q.getContent())
                    .setAuthor(authorResponse(q.getAuthor())))
        .orElse(null);
  }

  private static AuthorResponse authorResponse(Quote.Author author) {
    return Optional.ofNullable(author)
        .map(a -> new AuthorResponse().setFirstName(a.getFirstName()).setLastName(a.getLastName()))
        .orElse(null);
  }
}
