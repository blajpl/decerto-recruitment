package pl.blaj.decertorecruitment.common.persistence.entity;

import java.time.LocalDateTime;
import javax.persistence.MappedSuperclass;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@MappedSuperclass
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class AuditingEntity extends IdEntity {

  private Boolean deleted = Boolean.FALSE;

  private LocalDateTime deletedAt;
}
