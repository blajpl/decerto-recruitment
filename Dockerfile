FROM tomcat:9.0.39-jdk11

RUN rm -rf ${CATALINA_HOME}/webapps/examples
RUN rm -rf ${CATALINA_HOME}/webapps/host-manager
RUN rm -rf ${CATALINA_HOME}/webapps/manager
RUN rm -rf ${CATALINA_HOME}/webapps/docs
RUN rm -rf ${CATALINA_HOME}/webapps/root

COPY target/*.war /usr/local/tomcat/webapps/decerto-recruitment.war

EXPOSE 8080

CMD ["catalina.sh", "run"]
