# Decerto zadanie rekrutacyjne #

## Wymagania ##
* JDK 11
* Maven
* mariadb w wersji 10.5.8 lub nowszej
* docker (nie jest obowiązkowy ale zalecany)

## Instalacja ##
### Docker ###
Najprostszym i najszybszym sposobem na uruchomienie aplikacji jest skorzystanie z dockera.


1. Włączamy konsole i przechodzimy do głownego folderu z projektem
2. Wpisujemy komendę ``mvn clean install``
3. Wpisujemy komendę ``docker-compose build``
4. Wpisujemy komendę ``docker-compose up``
5. Aplikacja wstanie i będzie gotowa do działania pod adresem: ``http://localhost:8080/decerto-recruitment/``

UWAGA: docker do poprawnego działania potrzebuje wolnego portu ``8080``

### Lokalnie ###
1. Włączamy bazę mariadb
2. Wykonujemy na bazie skrypty sql z pliku ``init_database.sql``
3. Wpisujemy komendę ``mvn clean install``
4. W folderze tomcata edytujemy plik /conf/catalina.properties i na samym końcu dodajemy: ``spring.profiles.active=dev`` lub dodajemy ``-Dspring.profiles.active=dev`` do pliku startowego.
5. Deploy'ujemy w tomcacie plik ``/target/decerto-recruitment-1.0-SNAPSHOT.war`` (zmieniamy wpierw nazwę tego pliku na ``decerto-recruitment.war``)

## Endpointy ##
### Cytat (Quote) ###
#### Lista wszystkich cytatów ####
Adres: ``http://localhost:8080/decerto-recruitment/quote``

Typ: ``GET``

Ciało: puste

___

#### Pobieranie jednego cytatu po id ####
Adres: ``http://localhost:8080/decerto-recruitment/quote/{id}``, gdzie {id} -> identyfikator

Typ: ``GET``

Ciało: puste

___

#### Dodawanie nowego cytatu ####
Adres: ``http://localhost:8080/decerto-recruitment/quote``

Typ: ``POST``

Ciało (application/json):
```json
{
    "content": "Testowa treść",
    "author": {
        "firstName": "Adam",
        "lastName": "Maciejak"
    }
}
```

___

#### Edycja istniejącego cytatu ####
Adres: ``http://localhost:8080/decerto-recruitment/quote/{id}``, gdzie {id} -> identyfikator

Typ: ``PUT``

Ciało (application/json):
```json
{
    "content": "Testowa treść",
    "author": {
        "firstName": "Adam",
        "lastName": "Maciejak"
    }
}
```

___

#### Usuwanie istniejącego cytatu ####
Adres: ``http://localhost:8080/decerto-recruitment/quote/{id}``, gdzie {id} -> identyfikator

Typ: ``DELETE``

Ciało (application/json): puste