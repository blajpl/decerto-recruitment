-- For dev and prod
create database decerto;

create user 'decerto_app_user'@'localhost' identified by 'secretpassword';
grant all privileges on decerto.* to 'decerto_app_user'@'localhost';

create user 'decerto_liquibase_user'@'localhost' identified by 'secretpassword';
grant all privileges on decerto.* to 'decerto_liquibase_user'@'localhost';

flush privileges;